var  mongoose = require('mongoose'),
 hash = require('../common/cryptohash');
 register = require('./Model');
 admin = require('../admin/Model');
 transporter = require('../common/mailConfig');


//Save register
function create(req,res){
    var newPass = "";
    var result = req.body.email.toLowerCase();
    var hashEmail = hash.encrypt(result);
    var tokenVal = Math.floor(1000 + Math.random() * 9000);
    var IsActive = true;
    if(req.body.registerType === "email"){
         newPass = req.body.password;
         IsActive = false;
    }else if(req.body.registerType === "facebook"){
         newPass = req.body.facebookID
    }else if(req.body.registerType === "google"){
         newPass = req.body.googleID
    }   
    
    register.find({Email: hashEmail, RegisterType : req.body.registerType},function(err, success){
        if(success.length > 0){

            res.status(500).send({ success: false, message : 'Email is already registered. '  }); 

        }else{
            let registerData = new register({
                RegisterType : req.body.registerType,
                Email:  hashEmail,
                Password:   newPass,
                FacebookID : req.body.facebookID,
                GoogleID :  req.body.googleID,
                ImageURL : req.body.imageURL,
                First_name: req.body.firstName,
                Last_name: req.body.lastName,
                Contact: req.body.contact,
                D_O_B: req.body.dob,
                Gender: req.body.gender,
                Address_line_one: req.body.addressLineOne,
                Address_line_two: req.body.addressLineTwo,
                Country: req.body.country,
                State: req.body.state,
                Pincode: req.body.pinCode,              
                Marriage_status: req.body.marriageStatus,
                Ip_address: req.body.IpAddress,
                Token : tokenVal,
                Is_Active : IsActive,
                createdAt:  new Date(),
                updatedAt:  new Date()
            });

            registerData.save(function(err,resultData){
                resultData.Email = hash.decrypt(hashEmail);

            //-----------------send Mail---------------------------//    
            
            //var link = "http://localhost:3000/registerToken/"+ resultData.id +" varification token is ." + resultData.Token;
            var link = "https://panelreward.com/registerToken/"+ resultData.id +" varification token is ." + resultData.Token;
           
            messageData = ("Click that link to verify your account  "+ link );
            sendMail(req.body.email ,messageData,function(msg){
               
               
                if(msg.errno){
                    registerData.remove({ _id : resultData.id},function(err, success){
                        if(err){
                            res.json({  success: false, message : err });
                        }else{
                            res.json({  success: false, message : 'Please retry to register & check your email is valid or not' });
                        }
                        
                    });
                }else{
                    
                    res.status(200).send({ success: true, message: "Before you can login, you must active your account with the code sent to your email address. ", resultData });
                }
            });

                //test
                //res.status(200).send({ success: true, message: "Before you can login, you must active your account with the code sent to your email address. ", resultData });

			});
        }
    });
}


function register_mobile_primary(req,res){
    var newPass = "";
    var result = req.body.email.toLowerCase();
    var hashEmail = hash.encrypt(result);
    var tokenVal = Math.floor(1000 + Math.random() * 9000);



    if(req.body.email=="")
          {
            if(req.body.registerType === "facebook"){
                newPass = req.body.facebookID
                 
                   register.find({FacebookID: newPass, RegisterType : req.body.registerType},function(err, success){
                       if(success.length > 0){
               
                           res.status(500).send({ success: false, message : 'User already registered.'  }); 
               
                       }else{
                           let registerData = new register({
                               RegisterType : req.body.registerType,
                               Email:  hashEmail,
                               Password:   newPass,
                               FacebookID : req.body.facebookID,
                               GoogleID :  req.body.googleID,
                               First_name: req.body.first_name,
                               Last_name: req.body.last_name,
                               Token : tokenVal,
                               Is_Active : true,
                               DeviceType : req.body.DeviceType,
                               DeviceToken : req.body.DeviceToken,
                               DeviceID  : req.body.DeviceID,
                               RegisterStep : "1",
                               createdAt:  new Date(),
                               updatedAt:  new Date()
                           });
               
                           registerData.save(function(err,resultData){
                               resultData.Email = hash.decrypt(hashEmail);
                                   res.status(200).send({ success: true, message: "Your Primary Registeration is done.", resultData });
               
                           });
                        }
                   });
                   }                   
          }
          else
          {


            register.find({Email: hashEmail},function(err, success){
                if(success.length > 0){
                    res.status(500).send({ success: false, message : 'Email is already registered. ',resultData : success  }); 
                }
                else
                {

                       let registerData = new register({
                           RegisterType : req.body.registerType,
                           Email:  hashEmail,
                           Password:   req.body.password,
                           FacebookID : req.body.facebookID,
                           GoogleID :  req.body.googleID,
                           First_name: req.body.first_name,
                           Last_name: req.body.last_name,
                           Token : tokenVal,
                           Is_Active : true,
                           DeviceType : req.body.DeviceType,
                           DeviceToken : req.body.DeviceToken,
                           DeviceID  : req.body.DeviceID,
                           RegisterStep : "1",
                           createdAt:  new Date(),
                           updatedAt:  new Date()
                       });
           
                       registerData.save(function(err,resultData){
                           resultData.Email = hash.decrypt(hashEmail);
                               res.status(200).send({ success: true, message: "Your Primary Registeration is done.", resultData });
           
                       });
                   }
                });
         }
    }





// -- Register Secondary --//
function register_mobile_secondary(req,res){

            register.find({_id: req.body.userId},function(err, success){
                if(success.length > 0){

                    let registerData = new register({
                        Country: req.body.country, // update for mobile new change
                        Contact: req.body.contact,
                        D_O_B: req.body.dob,
                        Gender: req.body.gender,     
                        Marriage_status: req.body.marriageStatus,
                        RegisterStep : "2",
                        updatedAt:  new Date()
                    });
        
                    register.update({_id : req.body.userId},{ $set : {
                        Country: req.body.country,
                        Contact: req.body.contact,
                        D_O_B: req.body.dob,
                        Gender: req.body.gender,     
                        Marriage_status: req.body.marriageStatus,
                        RegisterStep : "2",
                        updatedAt:  new Date()
                    } },function(err,resultData){
                            res.status(200).send({ success: true, message: "Your Secoundry Registeration is done.", resultData });
                    });

                }
                else
                {
                   res.status(500).send({ success: false, message : 'This User is not registered with us. Please contact to support!' }); 
                }
                });
        
    }

//-- Register Mobile Final --//
function register_mobile_final(req,res){

    register.find({_id: req.body.userId},function(err, userData){
        if(userData.length > 0){

            register.update({_id : req.body.userId},{ $set : {
                Address_line_one: req.body.addressLineOne,
                Address_line_two: req.body.addressLineTwo,
                //Country: req.body.country,
                State: req.body.state,
                Pincode: req.body.pinCode,
                DeviceType : req.body.DeviceType,
                DeviceToken : req.body.DeviceToken,
                DeviceID  : req.body.DeviceID,
                RegisterStep : "3",
                updatedAt:  new Date()
            }},function(err,resultData){
                res.status(200).send({ success: true, message: "Your Registeration is done.", resultData, userData });
            });
        }
        else
        {
           res.status(500).send({ success: false, message : 'This User is not registered with us. Please contact to support!' }); 
        }
        });
}


//--  varifyToken --//
function varifyToken(req,res){
    register.find({_id : req.body.id},function(err, success){
        if(success && success.length>0){
            if(success[0].Token === req.body.token){

                register.update({_id : req.body.id} ,{ $set : { Is_Active : true } }, function (err, data){
                    if(err){
                        res.json({ success:false, message: err });
                    }else{
                        res.status(200).send({ success: true, message : "Your account has been activated successfully"});
                    }
                });

            }else{
                res.status(200).send({ success: false, message : "Token is not valid "});
            }
        }else{
            res.status(200).send({ success: false, message : "User is not exists "});
        }
    });   
}

//getAllUser
function getAllUser(req,res){
    admin.find({Token : req.params.token},function(err, success){
        if(success.length>0){
            
            register.find({}, function (err, data){
                if(err){
                    res.json({ success:false, message: err });
                }else{
                    for(var i=0; i<data.length; i++){
                        data[i].Email = hash.decrypt(data[i].Email);
                    }
                    res.status(200).send({ success: true, profileData : data});                    
                }
            });
        }else{
            res.status(200).send({ success: false, message : "admin is not valid "});
        }
    });   
}


function getUserCountByCountry(req, res){
    
    let countryArray = []; 

        register.aggregate([
            { "$match" : {}},
            // { "$match" : {Is_Active: true}},
                {"$group" : 
                {_id:{Country:"$Country"},count:{$sum:1}}
            },             
            {$sort:{"_id.Country":1}}
        ]).exec(function(err, totalCountryData){

        if(err){
            res.json({ success:false, message: err });
        }else{
            
            if(totalCountryData && totalCountryData.length>0){
                for(let i =0; i<totalCountryData.length; i++){
                    let members ={};
                    members.name = totalCountryData[i]._id.Country;
                    members.totalUsers =  totalCountryData[i].count; 
                    countryArray[i] = members

                    if( i === totalCountryData.length-1){
                        res.status(200).send({success: true, countryData : countryArray});
                    }
                }
            }else{
                    res.status(200).send({success: false, message: "Sorry you have not registered users"});
                }
        }
    });
}

function getUsersByCountry(req, res){
    let value = parseInt(req.params.startPoint);
    let skipValue = (value*5 - 5);
    
    register.find({Country : req.params.country}).skip(skipValue).limit(5).exec(function(err, usersList){
        if(err){
            res.json({ success:false, message: err });
        }else{
            if(usersList && usersList.length>0){
                res.status(200).send({success: true, usersList : usersList});
            }else{
                res.status(200).send({success: false, message: "Sorry, Users not found that Country"});
            }
        }
    });
}


//....send mail function
function sendMail(email, messageData , callback) {
    // setup email data with unicode symbols
    let mailOptions = {
        from: 'support@panelreward.com', // sender address
        to: email, // list of receivers
        subject: "New member token verify from Panel Reward",
        html: messageData 
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            callback(error);
            console.log(error);
        }
        if(info){
            console.log('Message sent: %s', info.messageId);
            var respo = ('Message sent: %s', info.messageId);
            callback (respo);
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info)); 
        }        
    });
}


//insert data manually of users..




module.exports = {create: create, varifyToken: varifyToken, getAllUser: getAllUser,
                    register_mobile_primary : register_mobile_primary,
                    register_mobile_secondary : register_mobile_secondary,
                    register_mobile_final : register_mobile_final, 
                    getUserCountByCountry : getUserCountByCountry,
                    getUsersByCountry : getUsersByCountry  };