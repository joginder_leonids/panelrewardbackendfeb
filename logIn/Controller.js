var loginUserModel = require('../registration/Model'),
    jwt = require('jsonwebtoken'),
    config = require('../config/config'),
    hash = require('../common/cryptohash'),
    transporter = require('../common/mailConfig'),
    userDisableModel = require('../userDisable/Model'),
    userAnsCountModel = require('../userAnsCount/Model');

module.exports.login = function(req,res){
    
    var result = req.body.email.toLowerCase();

    var hashEmail = hash.encrypt(result);
    if(req.body.registerType === "email"){

        loginUserModel.find({Email: hashEmail, Password: req.body.password},function(err, resultData){
       
            if(resultData.length > 0){
                if(resultData[0].Is_Active ===true){

                    let userID = resultData[0]._id;
                    let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                    let loginData = {
                        Email : req.body.email,
                        Password : req.body.password 
                    }
        
                    jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                       
                        res.json({
                        success: 'true',
                        msg: 'Login Successfull',
                        data: { userToken: token},
                        userID,
                        user_name,
                        resultData
                        });

                        insertUserDemographics( resultData[0].id);
                    });
                }else{
                    userDisableModel.find({user_id: resultData[0].id},function(err, userDisable){
                        if (err) {
                            res.json({ success: false, message: err });
                        }
                        if(userDisable.length>0){
                            res.json({ success: false, error: 'Your account is disabled. Please contact to support on support@panelreward.com to resolve the issue. Thank You' });
                        }
                        else {
                            res.json({ success: false, error: 'Your email id is not verified. Please verify your email id first.' });
                    
                        }
                    });
                }
            } else {
            res.status(200).send({ success: false, error : 'Invalid user name or password.' });
            }
        });
       
    }else if(req.body.registerType === "facebook"){

        loginUserModel.find({Email: hashEmail, FacebookID : req.body.facebookID, RegisterType : req.body.registerType },function(err, resultData){
            if(resultData.length > 0){
                let userID = resultData[0]._id;
                let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                let loginData = {
                    Email : req.body.email,
                    Password : resultData[0].password 
                  }
    
                  jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                    
                    res.json({
                      success: 'true',
                      msg: 'Login Successfull',
                      data: { userToken: token},
                      userID,
                      user_name 
                    });
                    insertUserDemographics( resultData[0].id);
                  })
            }else{
                var tokenVal = Math.floor(1000 + Math.random() * 9000);

                let registerData = new register({
                    RegisterType : req.body.registerType,
                    Email :  hashEmail,
                    Password :   req.body.facebookID,
                    FacebookID : req.body.facebookID,
                    ImageURL : req.body.imageURL,
                    First_name : req.body.firstName,
                    Last_name : req.body.lastName,
                    Token : tokenVal,
                    Is_Active : true,
                    createdAt :  new Date(),
                    updatedAt :  new Date()
                });

                registerData.save(function(err,result){
                    if(result){
                    loginUserModel.find({Email: hashEmail, FacebookID : req.body.facebookID, RegisterType : req.body.registerType },function(err, resultData){
                        if(resultData.length > 0){
                            let userID = resultData[0]._id;
                            let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                            let loginData = {
                                Email : req.body.email,
                                Password : resultData[0].password 
                            }
            
                            jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                               
                                res.json({
                                success: 'true',
                                msg: 'Login Successfull',
                                data: { userToken: token},
                                userID,
                                user_name 
                                });

                                insertUserDemographics( resultData[0].id);
                            });
                        }else {
                            res.status(200).send({ success: false, error : 'Invalid user name or password.' });
                            }
                    });
                }
                });
            }
        });
        
    }else if(req.body.registerType === "google"){
        
    }   
};


function insertUserDemographics(userID){
    
    userAnsCountModel.find({user_id : userID}, function(err, countData){
        if(countData.length <= 0){
            let userAnsCountData = new userAnsCountModel({
                user_id   : userID,
                cat_id_1  : 0,
                cat_id_2  : 0,
                cat_id_3  : 0,
                cat_id_4  : 0,
                cat_id_5  : 0,
                cat_id_6  : 0,
                cat_id_7  : 0,
                createdAt :new Date(),
                updatedAt : new Date()
                
            });

            userAnsCountData.save(function(err,result){

            });
        }

    });
}

module.exports.authUser = function(req,res){
    
    var result = req.body.email.toLowerCase();

    var hashEmail = hash.encrypt(result);
    if(req.body.registerType === "email"){

        loginUserModel.find({Email: hashEmail, Password: req.body.password},function(err, resultData){
       
            if(resultData.length > 0){
                if(resultData[0].Is_Active === true){

                    let userID = resultData[0]._id;
                    let registerStep = resultData[0].RegisterStep;
                    let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                    let loginData = {
                        Email : req.body.email,
                        Password : req.body.password 
                    }
        
                    loginUserModel.update({_id : userID},{ $set : {
                        DeviceType : req.body.DeviceType,
                        DeviceToken : req.body.DeviceToken,
                        DeviceID  : req.body.DeviceID,
                        updatedAt:  new Date()
                    } },function(err,resultData){
                        
                    });

                    jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                       
                        res.json({
                        success: 'true',
                        msg: 'Login Successfull',
                        registerationStep  : registerStep,
                        data: { userToken: token},
                        userID,
                        user_name 
                        });
                        insertUserDemographics( resultData[0].id);
                    });
                }else{
                   
                        userDisableModel.find({user_id: resultData[0]._id},function(err, userDisable){
                            if (err) {
                                res.json({ success: false, message: err });
                            }
                            if(userDisable.length>0){
                                res.json({ success: false, error: 'Your account is disabled. Please contact to support on support@panelreward.com to resolve the issue. Thank You' });
                            }
                            else {
                                res.json({ success: false, error: 'Your email id is not verified. Please verify your email id first.' });
                        
                            }
                        });
                    
                }
            } else {
            res.status(200).send({ success: false, error : 'Invalid user name or password.' });
            }
        });
       
    }else if(req.body.registerType === "facebook"){

        loginUserModel.find({FacebookID : req.body.facebookID},function(err, resultData){
            if(resultData.length > 0){
                if(resultData[0].Is_Active === true){
                    let userID = resultData[0]._id;
                    let registerStep = resultData[0].RegisterStep;
                    let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                    let loginData = {
                        Email : req.body.email,
                        Password : resultData[0].password 
                    }

                    loginUserModel.update({_id : userID},{ $set : {
                        DeviceType : req.body.DeviceType,
                        DeviceToken : req.body.DeviceToken,
                        DeviceID  : req.body.DeviceID,
                        updatedAt:  new Date()
                    } },function(err,resultData){
                        
                    });

                    jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                        
                        res.json({
                        success: 'true',
                        msg: 'Login Successfull',
                        registerationStep  : registerStep,
                        data: { userToken: token},
                        userID,
                        user_name 
                        });
                        insertUserDemographics( resultData[0].id);
                    });
                }else{
                   
                    userDisableModel.find({user_id: resultData[0]._id},function(err, userDisable){
                        if (err) {
                            res.json({ success: false, message: err });
                        }
                        if(userDisable.length>0){
                            res.json({ success: false, error: 'Your account is disabled. Please contact to support on support@panelreward.com to resolve the issue. Thank You' });
                        }
                        else {
                            res.json({ success: false, error: 'Your email id is not verified. Please verify your email id first.' });
                    
                        }
                    });
                }
            }else{
                var tokenVal = Math.floor(1000 + Math.random() * 9000);

                let registerData = new register({
                    RegisterType : req.body.registerType,
                    Email:  hashEmail,
                    Password:   req.body.password,
                    FacebookID : req.body.facebookID,
                    GoogleID :  req.body.googleID,
                    First_name: req.body.first_name,
                    Last_name: req.body.last_name,
                    Token : tokenVal,
                    Is_Active : true,
                    DeviceType : req.body.DeviceType,
                    DeviceToken : req.body.DeviceToken,
                    DeviceID  : req.body.DeviceID,
                    RegisterStep : "1",
                    createdAt:  new Date(),
                    updatedAt:  new Date()
                });
               
                registerData.save(function(err,result){
                    if(result){
                    loginUserModel.find({ FacebookID : req.body.facebookID, RegisterType : req.body.registerType },function(err, resultData){
                        if(resultData.length > 0){
                            let userID = resultData[0]._id;
                            let registerStep = resultData[0].RegisterStep;
                            let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                            let loginData = {
                                Email : req.body.email,
                                Password : resultData[0].password 
                            }
            
                            jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                                console.log('Working1');
                                res.json({
                                success: 'true',
                                msg: 'Login Successfull',
                                registerationStep  : registerStep,
                                data: { userToken: token},
                                userID,
                                user_name 
                                });
                                insertUserDemographics( resultData[0].id);
                            });
                        }else {
                            res.status(200).send({ success: false, error : 'Invalid user name or password.' });
                            }
                    });
                }
                });
            }
        });    
    }else if(req.body.registerType === "google"){
        loginUserModel.find({GoogleID : req.body.googleID},function(err, resultData){
            if(resultData.length > 0){
                if(resultData.length > 0){
                    let userID = resultData[0]._id;
                    let registerStep = resultData[0].RegisterStep;
                    let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                    let loginData = {
                        Email : req.body.email,
                        Password : resultData[0].password 
                    }
                    loginUserModel.update({_id : userID},{ $set : {
                        DeviceType : req.body.DeviceType,
                        DeviceToken : req.body.DeviceToken,
                        DeviceID  : req.body.DeviceID,
                        updatedAt:  new Date()
                    } },function(err,resultData){
                        
                    });
                    jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                       
                        res.json({
                        success: 'true',
                        msg: 'Login Successfull',
                        registerationStep  : registerStep,
                        data: { userToken: token},
                        userID,
                        user_name 
                        });
                        insertUserDemographics( resultData[0].id);
                    })
                }else{
                   
                    userDisableModel.find({user_id: resultData[0]._id},function(err, userDisable){
                        if (err) {
                            res.json({ success: false, message: err });
                        }
                        if(userDisable.length>0){
                            res.json({ success: false, error: 'Your account is disabled. Please contact to support on support@panelreward.com to resolve the issue. Thank You' });
                        }
                        else {
                            res.json({ success: false, error: 'Your email id is not verified. Please verify your email id first.' });
                    
                        }
                    });
                }
            }else{
                var tokenVal = Math.floor(1000 + Math.random() * 9000);

                let registerData = new register({
                    RegisterType : req.body.registerType,
                    Email:  hashEmail,
                    Password:   req.body.password,
                    FacebookID : req.body.facebookID,
                    GoogleID :  req.body.googleID,
                    First_name: req.body.first_name,
                    Last_name: req.body.last_name,
                    Token : tokenVal,
                    Is_Active : true,
                    DeviceType : req.body.DeviceType,
                    DeviceToken : req.body.DeviceToken,
                    DeviceID  : req.body.DeviceID,
                    RegisterStep : "1",
                    createdAt:  new Date(),
                    updatedAt:  new Date()
                });
    
                registerData.save(function(err,result){
                    if(result){
                    loginUserModel.find({ GoogleID : req.body.googleID, RegisterType : req.body.registerType },function(err, resultData){
                        if(resultData.length > 0){
                            let userID = resultData[0]._id;
                            let registerStep = resultData[0].RegisterStep;
                            let user_name = ( resultData[0].First_name + " " +resultData[0].Last_name );
                            let loginData = {
                                Email : req.body.email,
                                Password : resultData[0].password 
                            }
                            jwt.sign({token: loginData}, config.jwt_secret, (err, token) => {
                               
                                res.json({
                                success: 'true',
                                msg: 'Login Successfull',
                                registerationStep  : registerStep,
                                data: { userToken: token},
                                userID,
                                user_name 
                                });
                                insertUserDemographics( resultData[0].id);
                            })
                        }else {
                            res.status(200).send({ success: false, error : 'Invalid user name or password.' });
                            }
                    });
                }
                });
            }
        });
    }     
};


module.exports.resetPassword = function(req,res){

    //check email
   // var hashEmail = hash.encrypt(req.body.email);

    loginUserModel.find({ _id : req.body.id}).exec(function(err, data){
		if(err){
			res.json({ success:false, message: err });
		}else{
            if(data.length>0){
                register.update({ _id : req.body.id} ,{ $set : { Password : req.body.password } }, function (err, data){
                    if(err){
                        res.json({ success:false, message: err });
                    }else{
                        res.status(200).send({ success: true, data, message: "Password has been updated. " });
                    }
                });
            }else{
                res.status(200).send({ success: true, data, message: "Email is not register, please enter vaild email. " });
            }
        }
    });

}

//------------------------------------------------//
//--------------forgetPasswordMail ---------------//
//------------------------------------------------//

module.exports.forgetPasswordMail = function(req,res){
    //check email
    let messageData ="";
    var hashEmail = hash.encrypt(req.body.email);

    loginUserModel.find({ Email: hashEmail}).exec(function(err, data){
        if(err){
            res.json({ success:false, message: err });
        }else{
            if(data.length>0){
                var link = "https://panelreward.com/resetPassword/sn:"+data[0].id;   // Forget password link..
                //var link = "http://localhost:3000/resetPassword";   // Forget password link..
                messageData =  link ;
                sendMail(req.body.email,messageData,data[0].First_name,function(msg){
                    if(msg){
                        res.status(200).send({ success: true, message: "Message has been sent on register's email ",msg });
                    }
                });
                
            }else{
                res.status(200).send({ success: false, data, message: "Email is not register, please enter vaild email. " });
            }
        }
    });
}


//....send mail function
function sendMail(email, messageData , name, callback) {
    // setup email data with unicode symbols
    let mailOptions = {
        from: 'support@panelreward.com', // sender address
        to: email, // list of receivers
        subject: "Reset Password link for Panel Reward",
        html: '<html>\
                <body>\
                <p> Hello '+ name +'</p>\
                <h3>Welcome To Panel Reward.</h3>\
                <p> Hope You are doing great! <br/>\
                You have made forget password request on https://panelreward.com for '+ email +'   Please click on the below link to reset your password.\
                </p>\
                <h4> Password Reset Link : </h4>'+ messageData +' <br/>\
                <p>If you did not request for reset password then ignore this email.</p>\
                <p>Thanks <br/>\
                PANEL REWARD TEAM<p>\
                <p>note : please do not reply on this email.</p>\
                </body>\
                </html>' 
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            callback(error);
            console.log(error);
//res.status(500).send({ success: false, error });
        }
        if(info){
            console.log('Message sent: %s', info.messageId);
            var respo = ('Message sent: %s', info.messageId);
            callback (respo);
        // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info)); 
        //res.status(200).send({ success: true, message: " The link has been sent on your email " });
           
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        }
        
    });
}