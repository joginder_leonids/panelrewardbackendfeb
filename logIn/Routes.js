const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
        var mid = require('../Middleware/middleware');
        

// --------- Router --------

//Login user 

/*=============================================
=            USER LOGIN            =
=============================================*/
mappingRouter.route('/login')
        .post(mappingController.login)

mappingRouter.route('/authUser')
        .post(mappingController.authUser)
        
//This is an extra route.
//Logout can be done on front end by deleteing token
mappingRouter.get('/logout', mid.verifyToken, function(req, res){
        res.json({
          status: 'success',
          msg: 'User log out successfully',
          data: null
        })
      })

/*=============================================
=           RESET PASSWORD            =
=============================================*/      


mappingRouter.route('/resetPassword')
        .post(mappingController.resetPassword)

/*=============================================
=           FORGET PASSWORD MAIL       =
=============================================*/      
        
mappingRouter.route('/forgetPasswordMail')
        .post(mappingController.forgetPasswordMail)

module.exports = mappingRouter;