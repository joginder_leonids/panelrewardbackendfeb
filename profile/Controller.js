var mongoose = require('mongoose'),
hash = require('../common/cryptohash'),
 profileModel = require('../registration/Model');

 //Get all data of studies.

 function getProfileById(req,res){
	profileModel.find({_id : req.params.id}).exec(function(err, data){
		if(err){
			res.json({ success:false, message: err });
		}else{
            data[0].Email = hash.decrypt(data[0].Email);
         res.json({ profile : data });
        }
	});
};

function updateProfile(req, res){
    var hashEmail = hash.encrypt(req.body.email);
    profileModel.findById(req.params.id)
        .then((item) => {
        
            item.Email =  hashEmail,
            item.Password =   req.body.password,
            item.First_name = req.body.firstName,
            item.Last_name = req.body.lastName,
            item.Contact = req.body.contact,
            item.D_O_B = req.body.dob,
            item.Gender = req.body.gender,
            item.Address_line_one = req.body.Address_line_one,
            item.Address_line_two = req.body.Address_line_two,
            item.Country = req.body.Country,
            item.State = req.body.State,
            item.Pincode = req.body.Pincode,
            item.Marriage_status = req.body.marriageStatus,
            item.updatedAt =  new Date();

            item.save(function (err, data) {
            if (err) {
                res.status(404).send({ success: false, err });
            }else{
                // after upade then get profile data again
                profileModel.find({_id : req.params.id}).exec(function(err, data){
                    if(err){
                        res.json({ success:false, message: err });
                    }else{
                    data[0].Email = hash.decrypt(data[0].Email);
                    res.json({ success : true, profile : data, message : "Profile update successfully"});
                    }
                });
            }
        });
    });
};

module.exports = { getProfileById : getProfileById, updateProfile : updateProfile };
