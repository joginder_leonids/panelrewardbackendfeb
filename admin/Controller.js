var  mongoose = require('mongoose'),
 hash = require('../common/cryptohash');
 admin = require('./Model');


//Save register
function create(req,res){
	var result = req.body.email.toLowerCase();
    var hashEmail = hash.encrypt(result);
    var tokenVal = Math.floor(1000 + Math.random() * 9000);
    admin.find({Email: hashEmail},function(err, success){
        if(success.length > 0){
            res.status(404).send({ success: false, message : 'Email is already registered. '  }); 

        }else{
            let adminData = new admin({
                Email : hashEmail,
                Password : req.body.password,
                First_name : req.body.firstName,
                Last_name : req.body.lastName,
                D_O_B : req.body.dob,
                Gender : req.body.gender,
                Address : req.body.address,
                Token : tokenVal,
                Is_Active : true,
                createdAt :  new Date(),
                updatedAt :  new Date()
            });

            adminData.save(function(err,resultData){
                resultData.Email = hash.decrypt(hashEmail);
                res.status(200).send({ success: true, resultData });
			});
        }
    });
}


function adminLogin(req, res){
	var result = req.body.email.toLowerCase();
    var hashEmail = hash.encrypt(result);
   
    admin.find({Email: hashEmail, Password: req.body.password},{Email:1, Password:1, Token:1},function(err, resultData){
        
        if(resultData.length > 0){

            res.status(200).send({ success: true, data :resultData });
        }else{
            res.status(404).send({ success: false, message : 'User not exists ' }); 
        }
    });
}




module.exports = {create: create, adminLogin: adminLogin};