var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

    var userAnsCountModel = new Schema({
        user_id   : { type: Schema.Types.ObjectId , ref: 'register_users' },
        cat_id_1  : { type: Number },
        cat_id_2  : { type: Number },
        cat_id_3  : { type: Number },
        cat_id_4  : { type: Number },
        cat_id_5  : { type: Number },
        cat_id_6  : { type: Number },
        cat_id_7  : { type: Number },
        createdAt : { type: Date },
        updatedAt : { type: Date }
});


module.exports = mongoose.model('user_ans_counts', userAnsCountModel);