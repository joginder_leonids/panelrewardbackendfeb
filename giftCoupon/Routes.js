const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
        

// --------- Router --------



//----- Admin Routes -------//
    mappingRouter.route('/addCoupons')
    .post(mappingController.addCoupons)
     
    mappingRouter.route('/allCouponByAdmin/:token')
    .get(mappingController.allCouponByAdmin)

    mappingRouter.route('/getAllCouponCountByAdmin/:token')
    .get(mappingController.getAllCouponCountByAdmin)

    mappingRouter.route('/assignGiftcouponOnline')
    .post(mappingController.assignGiftcouponOnline)

    mappingRouter.route('/getGiftcouponByUser/:assignId')
    .get(mappingController.getGiftcouponByUser)
module.exports = mappingRouter;