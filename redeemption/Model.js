var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

var TransactionHistoryModel = new Schema({
    user_id : { type: Schema.Types.ObjectId , ref: 'register_users' },
    totalPoints : {type : String},
    totalAmount : {type : String},
    paymentType : {type : String},
    paypalEmailId : { type : String },
    deliveryType   : {type : String },
    assignGiftcouponId : { type: Schema.Types.ObjectId , ref: 'assigns_giftcoupons' },
    addressLineOne  :  {type : String},
    addressLineTwo : {type : String},
    state : {type : String},
    city: {type : String},
    pinCode  : {type : String},     
    paymentStatus  :   {type: String},
    paymentComments : {type: String},
    checkByAdminID : { type: Schema.Types.ObjectId , ref: 'admin_users' },
    checkByAdminName : {type: String},
    txn_id :  {type: String},
    txn_status :   {type: String},
    txn_error : {type: String},
    createdAt:  {type: Date },
    updatedAt:  {type: Date}
});

var rewardPonisMasterModel = new Schema({
  
    user_id   : {type : String},
    transactionHistory_id : { type: Schema.Types.ObjectId , ref: 'transactionhistories' },
    total_reward_point : {type: String},
    paymentType: {type : String},
    paymentStatus : {type: String},
    checkByAdminID : { type: Schema.Types.ObjectId , ref: 'admin_users' },
    checkByAdminName : {type: String},
    created_at  : {type : Date},
    updated_at  : {type : Date}
});

var transactionHistory =  mongoose.model('transactionHistory', TransactionHistoryModel);
var rewardPonisMaster =  mongoose.model('rewardPonisMaster', rewardPonisMasterModel);

module.exports = {

    transactionHistory  : transactionHistory,
    rewardPonisMaster : rewardPonisMaster
};