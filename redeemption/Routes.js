const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
        

// --------- Router --------

        //-------GET Route ------//
        mappingRouter.route('/fetchUserTraction/:id')
        .get(mappingController.fetchUserTraction)

        //----- POST Route -------//
        mappingRouter.route('/addTransaction')
        .post(mappingController.addTransaction)
     

// ---------- Admin Routes  --------//        
      
mappingRouter.route('/collectAllPaymentTransaction/:token')
.get(mappingController.collectAllPaymentTransaction)

mappingRouter.route('/collectPaymentTransactionByUser/:token/:tId')
.get(mappingController.collectPaymentTransactionByUser)

mappingRouter.route('/verifyPaypalTranaction')
.post(mappingController.verifyPaypalTranaction)

mappingRouter.route('/rejectPaypalTransaction')
.post(mappingController.rejectPaypalTransaction)

mappingRouter.route('/payWithPaypal')
.post(mappingController.payWithPaypal)

mappingRouter.route('/checkpayWithPaypal')
.post(mappingController.checkpayWithPaypal)



module.exports = mappingRouter;