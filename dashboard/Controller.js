var  mongoose = require('mongoose'),
 hash = require('../common/cryptohash');
 register = require('../registration/Model');
 admin = require('../admin/Model');
 studyModel = require('../study/Model');


//getDashboardData
function getDashboardData(req,res){
    let countData = {};
    admin.find({Token : req.params.token},function(err, success){
        if(success.length>0){
            
            register.find({Is_Active: true}).count().exec(function(err, data){

                if(err){
                    res.status(404).send({ success:false, message: err });
                }else{
                    countData.members = data;

                    studyModel.study.find({is_live: true}).count().exec(function(err, study){
                        if(err){
                            res.status(404).send({success:false, message: err });
                        }else{
                                countData.studies = study;
                                res.status(200).send({success:true, data:  countData}); 
                        }
                    });                
                }
            });
        }else{
            res.status(404).send({ success: false, message : "admin is not valid "});
        }
    });   
}




module.exports = { getDashboardData: getDashboardData};