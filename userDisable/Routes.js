const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
        



// --------- Router --------

//Register Route
        mappingRouter.route('/disabledUsers')
        .post(mappingController.disabledUsers)
        
        mappingRouter.route('/enabledUsers')
        .post(mappingController.enabledUsers)
     
     
        
       
module.exports = mappingRouter;