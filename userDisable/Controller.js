var  mongoose = require('mongoose'),
 userDisableModel = require('./Model'),
 registerModel = require('../registration/Model'),
 adminModel = require('../admin/Model');


//Save register
function disabledUsers(req,res){
   
    adminModel.find({ Email: req.body.adminMailId },function(err, adminValid){

        if(adminValid.length > 0){
            
            userDisableModel.find({user_id: req.body.userId},function(err, userExist){
                if (err) {
                    res.json({ success: false, message: err });
                }
                if(userExist.length > 0){
                    res.status(500).send({ success: false, message : 'User has already disabled.'  }); 
                }else{
                    registerModel.find({_id: req.body.userId},function(err, userRegistered){
                        if (err) {
                            res.json({ success: false, message: err });
                        }

                        if(userRegistered.length > 0){

                            let userData = new userDisableModel({
                                user_id : req.body.userId,
                                disable_by_id : adminValid[0].id,
                                disable_by : adminValid[0].First_name,
                                createdAt :  new Date(),
                                updatedAt :  new Date()
                            });

                            userData.save(function(err,resultData){
                                if (err) {
                                    res.json({ success: false, message: err });
                                }else{
                                    registerModel.update({_id : req.body.userId} ,{ $set : { Is_Active : false } }, function (err, data){
                                        if (err) {
                                            res.json({ success: false, message: err });
                                        }else{
                                            let value = parseInt(req.body.currentPage);
                                            let skipValue = (value*5 - 5);
                                            
                                            register.find({Country : req.body.countryName}).skip(skipValue).limit(5).exec(function(err, usersList){
                                                if(err){
                                                    res.json({ success:false, message: err });
                                                }else{
                                                   
                                                        res.status(200).send({success: true, usersList : usersList, message : 'User has been disabled successfully.'});
                                                    // }else{
                                                    //     res.status(200).send({success: false, message: "Sorry, Users not found that Country"});
                                                    // }
                                                }
                                            });
                                            //res.status(200).send({ success: true, message : 'User has been disabled successfully.' });
                                        }
                                    });
                                }
                            });
                        }else{
                            res.status(500).send({ success: false, message : 'User not found.'  }); 
                        }
                    });
                }
            });
        }else{
            res.status(500).send({ success: false, message : 'Invalid admin'  }); 
        }
    });
}

//enabledUsers
function enabledUsers(req, res){
    adminModel.find({ Email: req.body.adminMailId },function(err, adminValid){

        if(adminValid.length > 0){

            registerModel.find({_id: req.body.userId},function(err, userRegistered){
                if (err) {
                    res.json({ success: false, message: err });
                }

                if(userRegistered.length > 0){
                    if(userRegistered[0].Is_Active === true){
                        res.status(500).send({ success: false, message : 'User has been already active'  }); 
                    }else{

                        userDisableModel.find({user_id: req.body.userId},function(err, userExist){
                            if (err) {
                                res.json({ success: false, message: err });
                            }
                            if(userExist.length > 0){

                            userDisableModel.remove({user_id: req.body.userId},function(err, userExist){
                                if (err) {
                                    res.json({ success: false, message: err });
                                }else{

                                    registerModel.update({_id : req.body.userId} ,{ $set : { Is_Active : true } }, function (err, data){
                                        if (err) {
                                            res.json({ success: false, message: err });
                                        }else{
                                            let value = parseInt(req.body.currentPage);
                                            let skipValue = (value*5 - 5);
                                            
                                            register.find({Country : req.body.countryName}).skip(skipValue).limit(5).exec(function(err, usersList){
                                                if(err){
                                                    res.json({ success:false, message: err });
                                                }else{
                                                
                                                        res.status(200).send({success: true, usersList : usersList, message : 'User has been disabled successfully.'});
                                                        
                                                    // }else{
                                                    //     res.status(200).send({success: false, message: "Sorry, Users not found that Country"});
                                                    // }
                                                }
                                            });
                                            //res.status(200).send({ success: true, message : 'User has been disabled successfully.' });
                                        }
                                    });
                                }
                            });
                            }else{
                                res.status(500).send({ success: false, message : 'User is not verified by e-mail' }); 
                            }
                        });
                    }   
                }
            });
        }else{
            res.status(500).send({ success: false, message : 'Invalid admin'  }); 
        }
    });
}



module.exports = { disabledUsers: disabledUsers, enabledUsers : enabledUsers};