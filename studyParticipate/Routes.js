const   express = require('express'),
        mappingRouter = express.Router(),
        mappingController = require('./Controller');
        



// --------- Router --------

//Register Route
        mappingRouter.route('/study-participate/:id')
        .post(mappingController.create)
        
        mappingRouter.route('/study-participate-status')
        .post(mappingController.updateStudyParticipateStatus)

        mappingRouter.route('/study-participate-count/:id')
        .get(mappingController.countStudyParticipate)
     
        
       
module.exports = mappingRouter;