var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

	var studyParticipateModel = new Schema({
  
        s_id        : {type : String},
        p_id        : {type : String},
        completion_status   : {type : String},
        created_at  : {type : Date },
        updated_at  : {type : Date}
    });


module.exports = mongoose.model('study-participate',studyParticipateModel);