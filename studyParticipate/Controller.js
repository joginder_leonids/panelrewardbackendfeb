var mongoose = require('mongoose'),

studyParticipateModel = require('./Model');
studyModel = require('../study/Model');
TransactionHistory = require('../redeemption/Model');
var categoryModel = require('../profileDashbord/Model');
var userCatCountModel = require('../userAnsCount/Model');
var questions = categoryModel.questions;
var subQuestions = categoryModel.sub_questions;
var secSubQuestion = categoryModel.second_sub_questions;
var userAnswer = categoryModel.userAnswers;

//Save participation
function create(req,res){
    let participateData = new studyParticipateModel({
        s_id        :   req.body.studyId,
        p_id        :   req.params.id,    
        completion_status  :   req.body.statusId,
        createdAt   :   new Date(),
        updatedAt   :   new Date()
    });

    participateData.save(function(err,resultData){

        let studyMappingData = new studyModel.studyMapping({
            study_id        :   req.body.studyId,
            user_id        :   req.params.id,    
            completion_status  :   req.body.statusId,
            started :       new Date(),
            created_at   :   new Date(),
            updated_at   :   new Date()
        });

        studyMappingData.save( function (err, data){
            res.status(200).send({ success: true, resultData });
        });
    });      
}


//updateStudyParticipateStatus
function updateStudyParticipateStatus(req,res){
  

   studyModel.studyMapping.find({study_id: req.body.studyId , user_id: req.body.userId }).exec(function(err, checkStatudData){
    if(err){
        res.json({  success: false, error : err });
    }else{
            if(checkStatudData && checkStatudData.length>0){
                if(checkStatudData[0].completion_status === "0"){

                var statusEnd = "";
                if(req.body.status === "1" ){
                 statusEnd  = new Date();
                }else{
                 statusEnd  = null;
                }

                studyModel.studyMapping.update({ study_id: req.body.studyId , user_id: req.body.userId } ,{ $set : { completion_status :  req.body.status, ended : statusEnd } }, function (err, data){
                    if (err) {
                        res.status(200).send({ success: false, error: err });
                    }else{
                        studyParticipateModel.update({ s_id: req.body.studyId } ,{ $set : { completion_status :  req.body.status} }, function (err, data){
                            if (err) {
                                res.status(200).send({ success: false, error: err });
                            }else{

                                if(req.body.status =="1" ){
                                    studyModel.study.find({_id: req.body.studyId}).exec(function(err, Data){

                                        if(err){
                                            res.json({ success:false, message: err });
                                        }

                                        if(Data.length > 0){

                                            let studyCpi = Data[0].cpi;

                                            studyModel.rewardPoint.find({user_id : req.body.userId}).exec(function(err, oldRewardData){
                                            
                                                if(err){
                                                    res.json({ success:false, message: err });
                                                }

                                                if(oldRewardData && oldRewardData.length>0){
                                                
                                                    var newPoints = parseInt(oldRewardData[0].total_reward_point) + parseInt(studyCpi);

                                                    studyModel.rewardPoint.update({ user_id: req.body.userId } ,{ $set : { total_reward_point :  newPoints } }, function (err, data){
                                                        if(err){
                                                            res.json({ success:false, message: err });
                                                        }else{
                                                        res.status(200).send({ success: true, message : "status has been updated" });
                                                        }
                                                    });

                                                }else{

                                                    let rewardPointData = new studyModel.rewardPoint({
                                                    user_id        :   req.body.userId,
                                                    total_reward_point : studyCpi,
                                                    created_at   :   new Date(),
                                                    updated_at   :   new Date()
                                                    });

                                                    rewardPointData.save( function (err, data){
                                                        if(err){
                                                            res.json({ success:false, message: err });
                                                        }else{
                                                            res.status(200).send({ success: true, message : "status has been updated" });
                                                        }
                                                    });
                                               }
                                           });
                                        }
                                    });
                                }else{
                                    res.status(200).send({ success: true, message : "status has been updated" });
                                }
                            }
                        });
                    }
                });    

                }else{
                    res.json({  success: false, error : "You have already participated." });
                }
            }else{
                res.json({ success: false, error : "study participated not found." });
            }
        }
    });
}

// countStudyParticipate data :  studyCountData, last 5 study and transaction
function countStudyParticipate(req,res){

    //StudiesData
    studyModel.study.find({}).limit(5).sort( { created_at: -1 } ).exec(function(err, StudyData){

    if(err){
        res.status(404).send({ success: false, error : err });
    }
    
    if(StudyData){
        let str=JSON.stringify(StudyData);
        let StudiesData = JSON.parse(str);
        studyModel.studyMapping.find({user_id : req.params.id}).exec(function(err, studyMapData){
            if(studyMapData.length >0){
            for(var i=0; i<StudiesData.length ; i++){
                for(var j = 0; j<studyMapData.length; j++){
                    if(studyMapData[j].study_id === StudiesData[i]._id){
                        
                        StudiesData[i].completionStatus = studyMapData[j].completion_status;  
                    }
                }
            } 
        }

            TransactionHistory.transactionHistory.find({user_id : req.params.id}).limit(10).sort( { updatedAt : -1 } ).exec(function(err, userTransactionData){
                if(err){
                    res.json({success: false, error : err });
                }else{
                
            //get Demographics Complete Data

            gettotalQuestionProfileDashboard(req.params.id , function(totalQuestionByCat){
                if(totalQuestionByCat>= 0){
                
                    studyModel.studyMapping.find({user_id : req.params.id }).exec(function(err, data){
                        if(err){
                            res.status(404).send({ success: false, error : err });
                        }
                        studyModel.study.find({}).count(function(err, count){
                            if(err){
                                res.json({ success:false, message: err });
                            }
                            if(count){

                        let StudyCountData = {};
                            let totalStudy = count;
                            let startStudy = 0;
                            let completeStudy = 0;
                            let quotaFailStudy = 0;
                            let failStudy = 0;
                            let notParcipateStudy = 0;
                            let totalRewards = 0;
                            let CompletedStudy=[];

                        if(data.length>0){          
                            for(var i=0; i<data.length; i++){

                                if(data[i].completion_status === "0"){
                                    startStudy += 1;
                                }
                                if(data[i].completion_status === "1"){
                                    completeStudy += 1;
                                    CompletedStudy.push(data[i].study_id);
                                }
                                if(data[i].completion_status === "2"){
                                    quotaFailStudy += 1;
                                }
                                if(data[i].completion_status === "3"){
                                    failStudy += 1;
                                }
                                if((data[i].completion_status === "") || (data[i].completion_status === null) ){
                                    notParcipateStudy += 1;
                                }
                            }

                            //get total cpi
                            if(CompletedStudy && CompletedStudy.length > 0 ){
                                // getTotalCpiData(CompletedStudy ,function(cpiData){
                                //     if(cpiData){
                                //         for(var rew = 0 ; rew<cpiData.length; rew ++){
                                //             totalRewards = (totalRewards +  (parseInt(cpiData[rew].cpi))); 
                                //         }
                                studyModel.rewardPoint.find({user_id : req.params.id}).exec(function(err, data){
                                
                                    StudyCountData.totalStudy = totalStudy;
                                    StudyCountData.startStudy = startStudy;
                                    StudyCountData.completeStudy = completeStudy;
                                    StudyCountData.quotaFailStudy = quotaFailStudy;
                                    StudyCountData.failStudy = failStudy;
                                    StudyCountData.notParcipateStudy = notParcipateStudy;
                                    StudyCountData.totalRewards = data[0].total_reward_point;
                                    res.status(200).send({ success: true, StudyCountData, relativeData:{StudiesData, userTransactionData, totalQuestionByCat} });
                                });
                                // }
                            // });
                            }else{
                                StudyCountData.totalStudy = totalStudy;
                                StudyCountData.startStudy = startStudy;
                                StudyCountData.completeStudy = completeStudy;
                                StudyCountData.quotaFailStudy = quotaFailStudy;
                                StudyCountData.failStudy = failStudy;
                                StudyCountData.notParcipateStudy = notParcipateStudy;
                                StudyCountData.totalRewards = totalRewards;
                                res.status(200).send({ success: true, StudyCountData, relativeData:{StudiesData, userTransactionData, totalQuestionByCat} });
                            }
                        }else{
                            StudyCountData.totalStudy = totalStudy;
                            StudyCountData.startStudy = startStudy;
                            StudyCountData.completeStudy = completeStudy;
                            StudyCountData.quotaFailStudy = quotaFailStudy;
                            StudyCountData.failStudy = failStudy;
                            StudyCountData.notParcipateStudy = notParcipateStudy;
                            StudyCountData.totalRewards = totalRewards;
                            res.status(200).send({ success: true,StudyCountData, relativeData:{StudiesData, userTransactionData, totalQuestionByCat} });
                        }
                    }
                    });
                });
                }
                });
                }
            });
        });
    }
    });
    
}

// gettotalQuestionProfileDashboard
function gettotalQuestionProfileDashboard( userID,  callBack){

    userCatCountModel.find({user_id : userID}).exec(function(err, catCountData){
        
        console.log(catCountData);

        let total = parseInt(catCountData[0].cat_id_1 + catCountData[0].cat_id_2 +catCountData[0].cat_id_3 +catCountData[0].cat_id_4 +catCountData[0].cat_id_5 + catCountData[0].cat_id_6 + catCountData[0].cat_id_7)
        let peraentage =  Math.round((total*100)/700);

    return  callBack(peraentage);
    });
}



// function gettotalQuestionProfileDashboard( userID,  callBack){
//     let dashboardData={};
//     let quesCounts = 0;
//     let subQuesCounts = 0;
//     let secSubQuesCounts = 0;
//     let ansCounts = 0;
//     questions.find({status : true}).count(function(err, quesCount){
//         if(quesCount){
//             quesCounts = parseInt(quesCount);
//         }
//         subQuestions.find({status : true}).count(function(err, subQuesCount){
//             if(subQuesCount){
//                 subQuesCounts = parseInt(subQuesCount);
//             }
//             secSubQuestion.find({status : true}).count(function(err, secSubQuesCount){
//                 if(secSubQuesCount){
//                     secSubQuesCounts = parseInt(secSubQuesCount);
//                 }
//                 userAnswer.find({user_id : userID, status: true}).count(function(err, ansCount){
//                     if(ansCount){
//                         ansCounts = parseInt(ansCount);
//                     }
//                     dashboardData.totalQuestion = quesCounts + subQuesCounts + secSubQuesCounts
//                     dashboardData.totalUserAns = ansCounts;
//                     return  callBack(dashboardData);
//                 });
                
//             });
//         });
//     });
// }

// function getTotalCpiData(CompletedStudy, cpiData){
//     let Options = [];
//     let tempPromise=[];
//     for (key in CompletedStudy) {
    
//         let sid = CompletedStudy[key];
        
//         // Return new promise 
//         tempPromise.push( new Promise(function(resolve, reject) {

//             // Do async job
//             studyModel.study.find({_id : sid }).exec(function(err, data){
                
//                 Options[key] =  data[0];
//                 resolve(Options[key]);
//             });     
//         }));
//     }

//     Promise.all(tempPromise)
//     .then(values => { 
//         cpiData( values);
//     });
// }

module.exports = {create : create, updateStudyParticipateStatus : updateStudyParticipateStatus, countStudyParticipate : countStudyParticipate};