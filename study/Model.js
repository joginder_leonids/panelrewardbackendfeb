var mongoose = require('mongoose'),

    Schema = mongoose.Schema;

var studyModel = new Schema({

    study_name   : {type : String},
    study_url   : {type : String},
    is_live     : {type : Boolean},
    tId         : {type : String},
    modified    : {type : String},
    executed_by : {type : String},
    modified_by : {type : String},
    cpi         : {type : String},
    totalInvities  : {type : String},
    created_at  : {type : Date },
    updated_at  : {type : Date}
});

var studyUserMappingModel = new Schema({

    user_id           : {type : String},
    study_id          : {type : String},
    started           : {type : Date},
    ended             : {type : Date},
    completion_status : {type : String},
    earning           : {type : String},
    created_at        : {type : Date },
    updated_at        : {type : Date}
});

var alluserModel= new Schema({

    
    study_id  :{type:String},
    user_id  :{type:String},
    username  :{type:String},
    emailid  :{type:String},
    Created_at  :{type:Date},
    update_at   :{type:Date}
});

var rewardModel = new Schema({
  
    user_id   : {type : String},
    total_reward_point : {type: String},
    created_at  : {type : Date},
    updated_at  : {type : Date}
});



var study =  mongoose.model('study', studyModel);
var studyMapping = mongoose.model('studyMapping',studyUserMappingModel);
var alluser = mongoose.model('allusers',alluserModel);
var rewardPoint =  mongoose.model('rewardPoint', rewardModel);

module.exports = {

    study        : study,
    studyMapping : studyMapping,
    alluser : alluser,
    rewardPoint: rewardPoint
};