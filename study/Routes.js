const   express = require('express'),
        mappingRouter = express.Router(),
        mid = require('../Middleware/middleware'),
        mappingController = require('./Controller');

// --------- Router --------

//Register Route
mappingRouter.route('/createStudy')
        .post(mappingController.create)

mappingRouter.route('/updateStudy')
        .post(mappingController.updateStudy)

mappingRouter.route('/getAllStudy')
        .get(mappingController.getAllStudiesList)      
        
mappingRouter.route('/getStudyById/:id')
        .get(mappingController.getStudyListById)     
         
         //pagination routes
mappingRouter.route('/getStudyByIdPageNo/:id/:pageNo')
        .get(mappingController.getStudyByIdPageNo)     
        //..............
mappingRouter.route('/getViewInvites/:sid')
        .get(mappingController.getViewInvites)       

mappingRouter.route('/getStudyCountDetails/:uid')
        .get(mappingController.getStudyCountDetails)          
       
       // mappingRouter.route('/getStudyById/:id').get( mid.verifyToken, mappingController.getStudyListById) 
       
module.exports = mappingRouter;