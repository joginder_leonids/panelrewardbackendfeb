var mongoose = require('mongoose'),

 studyModel = require('./Model'),
 registerModel = require('../registration/Model');

//Save register

function create(req, res) {
    studyModel.study.find({ study_url: req.body.study_url }).exec(function (err, Data) {

        if (err) {
            res.json({ success: false, message: err });
        }
        if ( Data && Data.length > 0) {
            res.status(404).send({ success: false, message: 'study url already exists. ' });
        } else {

            var link = "https://my.panelreward.com/resetPassword/sn";   // Forget password link..
            //var link = "http://localhost:3000/resetPassword";   // Forget password link..
            let messageData = link;

            register.find({}, { Email: 1, First_name: 1,Last_name: 1, _id: 1 }, function (err, data) {

                let allEmailIDs = [];
                for (var i = 0; i < data.length; i++) {
                     data[i].Email = hash.decrypt(data[i].Email);
                    //allEmailIDs.push("dy.leonids@gmail.com");
                    allEmailIDs.push(data[i].Email);
                }

               // console.log("email", allEmailIDs);

                let studyData = new studyModel.study({
                    study_name: req.body.study_name,
                    study_url: req.body.study_url,
                    is_live: req.body.is_live,
                    tId: 'tid',
                    modified: new Date(),
                    executed_by: '',
                    modified_by: 'admin',
                    cpi: req.body.cpi,
                    totalInvities : ""+allEmailIDs.length,
                    created_at: new Date(),
                    updated_at: new Date()
                });

                let totalCountOfTheLoop = Math.ceil(allEmailIDs.length / 10);

                for (var mailCount = 0; mailCount < totalCountOfTheLoop; mailCount++) {

                    sendMail(allEmailIDs[mailCount], messageData, "Panelreward User", function (msg) {
                        if (msg) {
                        }
                    });

                    if (mailCount == totalCountOfTheLoop - 1) {
                        studyData.save(function (err, studyResultData) {

                            for (var i = 0; i < data.length; i++) {
                                 //data[i].Email = hash.decrypt(data[i].Email);
                               // allEmailIDs.push("dy.leonids@gmail.com");
            
                                let userData = new studyModel.alluser ({
                                    study_id : studyResultData._id,
                                    user_id: data[i]._id,
                                    username: data[i].First_name +" "+ data[i].Last_name,
                                    //emailid: hash.decrypt(data[i].Email),
                                    emailid: data[i].Email,
                                    created_at: new Date(),
                                    updated_at: new Date()
                                });
                    
                                userData.save(function (err, result) {

                                });
            
                            }

                            res.status(200).send({ success: true, studyResultData, message: 'Study insert successfull' });

                        });
                    }

                }

            });

      //     studyData.save(function(err,studyResultData){

            //     res.status(200).send({ success: true, studyResultData, message: 'Study insert successfull' });
            // });
        }
    });
}




//Get all data of studies.

function getAllStudiesList(req,res){
	studyModel.study.find({}).exec(function(err, data){
        if(err){
            res.status(404).send({ success: false, error: err});
        }
        if(data.length>0){
            res.status(200).send({ success: true, studyList: data});
        }else{
            res.status(404).send({ success: false, message:"Study data not found"});
        }
        
	});
};



  function getStudyListById(req,res){
  
    studyModel.study.find({}).sort( { created_at: -1 } ).exec(function(err, Data){
        if(err){
            res.json({ success:false, message: err });
        }
        if(Data.length>0){
            let str=JSON.stringify(Data);
            let studyData = JSON.parse(str);
            studyModel.studyMapping.find({user_id : req.params.id}).exec(function(err, studyMapData){
                if(studyMapData.length >0){
                for(var i=0; i<studyData.length ; i++){
                    for(var j = 0; j<studyMapData.length; j++){
                        if(studyMapData[j].study_id === studyData[i]._id){
                            
                            studyData[i].completionStatus = studyMapData[j].completion_status;  
                         }
                    }
                }
                res.json({ success:true, studyData});
            }else{
                res.json({ success:true, studyData});
            }
            });


        }
    //	res.json({ success:true, studyData});
	});
}


function updateStudy(req,res){

    studyModel.study.update({ _id: req.body.studyID} ,
        { $set : { study_name : req.body.study_name,
                    study_url : req.body.study_url,
                    is_live : req.body.is_live,
                    cpi : req.body.cpi,
                    tId : "tId" } }, function (err, data){
        if (err) {
            res.status(404).send({ success: false, error: err });
        }else{
            res.status(200).send({ success: true, message : 'Study has been updated.'});
        }
    });
}


//....send mail function
function sendMail(email, messageData, name, callback) {
    // setup email data with unicode symbols
    let mailOptions = {
        from: 'support@panelreward.com', // sender address
        to: email, // list of receivers
        subject: "New Study Created in Panel Reward",
        html: '<html>\
                <body>\
                <p> Hello '+ name + '</p>\
                <h3>Welcome To Panel Reward.</h3>\
                <p> Hope You are doing great! <br/>\
                <p> A New study is created. Please check from here : https://panelreward.com/ <br/>\
                \
                <p>Thanks <br/>\
                PANEL REWARD TEAM<p>\
                <p>note : please do not reply on this email.</p>\
                </body>\
                </html>'
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            callback(error);
            console.log(error);
            //res.status(404).send({ success: false, error });
        }
        if (info) {
            console.log('Message sent: %s', info.messageId);
            var respo = ('Message sent: %s', info.messageId);
            callback(respo);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            //res.status(200).send({ success: true, message: " The link has been sent on your email " });

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        }

    });
}

function getViewInvites(req, res){
  
    studyModel.alluser.find({study_id: req.params.sid}).exec(function (err, data) {
        if (err) {
            res.status(404).send({ success: false, error: err });
        }
        if (data.length > 0) {
            res.status(200).send({ success: true, invitesList: data });
        } else {
            res.status(404).send({ success: false, message: "Invites data not found" });
        }
    });
}
//pagination 
function getStudyByIdPageNo(req,res){
    let value = parseInt(req.params.pageNo);
    let skipValue = (value*10 - 10);
    let userID = req.params.id;
    studyModel.study.find({}).skip(skipValue).limit(10).sort( { created_at: -1 } ).exec(function(err, Data){
        
        if(err){
            res.json({ success:false, message: err });
        }
        if(Data.length>0){
            studyModel.study.find({}).count().exec(function(err, totalStudy){
                if(err){
                    res.json({ success:false, message: err });
                }
                let str=JSON.stringify(Data);
                let studyData = JSON.parse(str);
                studyModel.studyMapping.find({user_id : req.params.id}).exec(function(err, studyMapData){
                    if(studyMapData.length >0){
                    for(var i=0; i<studyData.length ; i++){
                        for(var j = 0; j<studyMapData.length; j++){
                            if(studyMapData[j].study_id === studyData[i]._id){
                                
                                studyData[i].completionStatus = studyMapData[j].completion_status;  
                            }
                        }
                    }                   
                    res.json({ success:true, studyData, totalStudy});
                }else{
                    res.json({ success:true, studyData, totalStudy});
                }
                });
            });
        }
	});
}

function getStudyCountDetails(req, res){
    studyModel.study.find({}).count(function(err, totalStudy){
        if(err){
            res.json({ success:false, message: err });
        }
        if(totalStudy){
            studyModel.studyMapping.find({user_id : req.params.uid}).count(function(err, completeStudyCount){
                if(err){
                    res.json({ success:false, message: err });
                }
                if(completeStudyCount){
                    res.status(200).send({ success: true, totalStudy, completeStudyCount});
                }   
            });
        }
    });
}

 module.exports = {create : create , 
                updateStudy: updateStudy,  
                getViewInvites: getViewInvites,
                getAllStudiesList : getAllStudiesList, 
                getStudyListById : getStudyListById,
                getStudyByIdPageNo : getStudyByIdPageNo,
                getStudyCountDetails : getStudyCountDetails};

