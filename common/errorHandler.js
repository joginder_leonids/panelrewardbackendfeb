
// Function Error handler for request processor

module.exports = function handleError(err) {
    if (err.code == 1100)
        return { success: false, message: 'Data already exists: ' };
    else {
        if (err.errors) {
            return { success: false, message: err.errors };
        }
        else {
            return { success: false, message: 'could not persist data. Error: ', err };
        }
    }
}